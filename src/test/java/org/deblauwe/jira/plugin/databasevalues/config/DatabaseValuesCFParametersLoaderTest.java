package org.deblauwe.jira.plugin.databasevalues.config;

import com.atlassian.jira.issue.fields.CustomField;
import junit.framework.TestCase;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Wim Deblauwe
 */
public class DatabaseValuesCFParametersLoaderTest extends TestCase
{
	public void testLoadingParams()
	{

		DatabaseValuesCFParameters parameters = DatabaseValuesCFParametersLoader.loadParameters( createCustomField( 10000L ) );
		assertNotNull( parameters );
		assertEquals( "jdbc:hsqldb:mem:plugintestdb", parameters.getDatabaseConnectionUrl() );
		assertEquals( "org.hsqldb.jdbcDriver", parameters.getDatabaseDriver() );
		assertEquals( "secret", parameters.getDatabasePassword() );
		assertEquals( "sa", parameters.getDatabaseUser() );
		assertEquals( DatabaseValuesCFParameters.EDIT_TYPE_COMBOBOX, parameters.getEditType() );
		assertEquals( 0, parameters.getPrimaryKeyColumnNumber() );
		assertEquals( "{2}, {1}<br/>{3}, {4}", parameters.getRenderingEditPattern() );
		assertEquals( "{1} {2} ({0})", parameters.getRenderingSearchPattern() );
		assertEquals( "{1} {2} from <a href=\"http://maps.google.com/maps?f=q&hl=nl&geocode=&q={3}, {4}\">{3}, {4}</a>", parameters.getRenderingViewPattern() );
		assertEquals( "select id, firstname, lastname, city, country from customer", parameters.getSqlQuery() );
		assertEquals( DatabaseValuesCFParametersLoader.DEFAULT_CACHE_TIMEOUT, parameters.getCacheTimeout() );
	}

	public void testAJAXStyleSelected()
	{
		DatabaseValuesCFParameters parameters = DatabaseValuesCFParametersLoader.loadParameters( createCustomField( 10010L ) );
		assertNotNull( parameters );
		assertEquals( "jdbc:hsqldb:mem:plugintestdb", parameters.getDatabaseConnectionUrl() );
		assertEquals( "org.hsqldb.jdbcDriver", parameters.getDatabaseDriver() );
		assertEquals( "secret", parameters.getDatabasePassword() );
		assertEquals( "sa", parameters.getDatabaseUser() );
		assertEquals( DatabaseValuesCFParameters.EDIT_TYPE_AJAX_INPUT, parameters.getEditType() );
		assertEquals( 0, parameters.getPrimaryKeyColumnNumber() );
		assertEquals( "{2}, {1}<br/>{3}, {4}", parameters.getRenderingEditPattern() );
		assertEquals( "{1} {2} ({0})", parameters.getRenderingSearchPattern() );
		assertEquals( "{1} {2} from <a href=\"http://maps.google.com/maps?f=q&hl=nl&geocode=&q={3}, {4}\">{3}, {4}</a>", parameters.getRenderingViewPattern() );
		assertEquals( "select id, firstname, lastname, city, country from customer", parameters.getSqlQuery() );
		assertEquals( DatabaseValuesCFParametersLoader.DEFAULT_CACHE_TIMEOUT, parameters.getCacheTimeout() );
	}

	public void testNoEditTypeConfigured()
	{
		DatabaseValuesCFParameters parameters = DatabaseValuesCFParametersLoader.loadParameters( createCustomField( 10020L ) );
		assertNotNull( parameters );
		assertEquals( "jdbc:hsqldb:mem:plugintestdb", parameters.getDatabaseConnectionUrl() );
		assertEquals( "org.hsqldb.jdbcDriver", parameters.getDatabaseDriver() );
		assertEquals( "secret", parameters.getDatabasePassword() );
		assertEquals( "sa", parameters.getDatabaseUser() );
		assertEquals( DatabaseValuesCFParameters.EDIT_TYPE_COMBOBOX, parameters.getEditType() );
		assertEquals( 0, parameters.getPrimaryKeyColumnNumber() );
		assertEquals( "{2}, {1}<br/>{3}, {4}", parameters.getRenderingEditPattern() );
		assertEquals( "{1} {2} ({0})", parameters.getRenderingSearchPattern() );
		assertEquals( "{1} {2} from <a href=\"http://maps.google.com/maps?f=q&hl=nl&geocode=&q={3}, {4}\">{3}, {4}</a>", parameters.getRenderingViewPattern() );
		assertEquals( "select id, firstname, lastname, city, country from customer", parameters.getSqlQuery() );
		assertEquals( DatabaseValuesCFParametersLoader.DEFAULT_CACHE_TIMEOUT, parameters.getCacheTimeout() );
	}

	public void testCacheTimeoutSet()
	{
		DatabaseValuesCFParameters parameters = DatabaseValuesCFParametersLoader.loadParameters( createCustomField( 10030L ) );
		assertNotNull( parameters );
		assertEquals( "jdbc:hsqldb:mem:plugintestdb", parameters.getDatabaseConnectionUrl() );
		assertEquals( "org.hsqldb.jdbcDriver", parameters.getDatabaseDriver() );
		assertEquals( "secret", parameters.getDatabasePassword() );
		assertEquals( "sa", parameters.getDatabaseUser() );
		assertEquals( DatabaseValuesCFParameters.EDIT_TYPE_COMBOBOX, parameters.getEditType() );
		assertEquals( 0, parameters.getPrimaryKeyColumnNumber() );
		assertEquals( "{2}, {1}<br/>{3}, {4}", parameters.getRenderingEditPattern() );
		assertEquals( "{1} {2} ({0})", parameters.getRenderingSearchPattern() );
		assertEquals( "{1} {2} from <a href=\"http://maps.google.com/maps?f=q&hl=nl&geocode=&q={3}, {4}\">{3}, {4}</a>", parameters.getRenderingViewPattern() );
		assertEquals( "select id, firstname, lastname, city, country from customer", parameters.getSqlQuery() );
		assertEquals( 1000L, parameters.getCacheTimeout() );
	}

	public void testJqlQueries()
	{
		DatabaseValuesCFParameters parameters = DatabaseValuesCFParametersLoader.loadParameters( createCustomField( 10040L ) );
		assertNotNull( parameters );
		String query = parameters.getQuery( "Country", "Belgium" );
		assertEquals( "select id from customer where country = 'Belgium'", query );

		query = parameters.getQuery( "Lastname", "Deblauwe" );
		assertEquals( "select id from customer where lastname = 'Deblauwe'", query );

		query = parameters.getQuery( "Blabla", "www" );
		assertNull( query ); //Since the query is not defined for Blabla, there should be no query returned
	}

	public void testConnectionToInternalJiraDatabase()
	{
		DatabaseValuesCFParameters parameters = DatabaseValuesCFParametersLoader.loadParameters( createCustomField( 10050L ) );
		assertNotNull( parameters );
		assertTrue( parameters.isUseInternalJiraDatabase() );
	}

	private CustomField createCustomField( long customFieldId )
	{
		CustomField customField = mock( CustomField.class );
		when( customField.getIdAsLong() ).thenReturn( customFieldId );
		return customField;
	}
}
